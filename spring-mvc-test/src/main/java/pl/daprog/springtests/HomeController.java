package pl.daprog.springtests;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/fb")
public class HomeController {
	
	private final String APP_ID;
	
	public HomeController(@Value("${spring.social.facebook.app-id}") String appId) {
		this.APP_ID = appId;
	}

	@RequestMapping(value = "/get-code", method = RequestMethod.GET)
	@ResponseBody
	String home() {
		return "Hello World, I'm "+APP_ID;
	}
}
