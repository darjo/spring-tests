package pl.daprog.springtests.aop;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.daprog.springtests.aop.aspect.PerformanceMonitor;
import pl.daprog.springtests.aop.service.CustomerService;

@Configuration
public class Application {
    
    public static void main(String[] args) {
    	
    	PropertyConfigurator.configure(Application.class.getResourceAsStream("/log4j.properties"));

    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
    	
    	CustomerService customerService = context.getBean(CustomerService.class);
    	
    	for(int i=0; i < 10; i++) {
	    	Long id = customerService.insert("Customer nr 1");
	    	String res = customerService.findById(id);
	    	String res2 = customerService.findByName(res);
    	}
    	
        context.close();
        
        PerformanceMonitor.dumpStatistics();
    }

}
