package pl.daprog.springtests.aop.service;

import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl extends AbstractService implements CustomerService {

	@Override
	public Long insert(String param) {
		
		doRandomSleep(4);
		return 0L;
	}
	
	@Override
	public String findById(Long id) {

		doRandomSleep(1);
		return "Customer id="+id;
	}
	
	@Override
	public String findByName(String name) {

		doRandomSleep(1);
		return "Customer name="+name;
	}
	
	private void doRandomSleep(long multi) {
		try {
			Thread.sleep(multi * (long) (Math.random()*100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
