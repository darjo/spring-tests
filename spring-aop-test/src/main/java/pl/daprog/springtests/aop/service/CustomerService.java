package pl.daprog.springtests.aop.service;

import org.springframework.stereotype.Service;

public interface CustomerService {
	
	Long insert(String param);
	
	String findById(Long id);
	
	String findByName(String name);

}
