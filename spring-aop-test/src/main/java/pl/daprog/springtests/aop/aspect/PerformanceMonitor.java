package pl.daprog.springtests.aop.aspect;

import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

/**
 * Aspect with annotation driven configuration
 * 
 * @author Darek
 *
 */
@Aspect
@Component
public class PerformanceMonitor {

	@Pointcut("execution(* pl.daprog.springtests.aop.service.*.*(..))")
	private void allServiceMethods() {}

	/*
	 * Method is invoked @Before any service method
	 */
	@Around("allServiceMethods()")
	public void monitor(ProceedingJoinPoint joinPoint) throws Throwable {
		
		Monitor monitor = MonitorFactory.start(getLabel(joinPoint));
		monitor.start();
		try {
			
			joinPoint.proceed();
			
		} catch (Throwable e) {
			throw e;
		} finally {
			monitor.stop();
		}

	}

	private String getLabel(JoinPoint joinPoint) {
		return joinPoint.getSignature().toShortString();
	}

	private static void log(Monitor monitor) {

		StringBuffer sb = new StringBuffer();
		sb.append(monitor.getLabel());
		sb.append(" Hits:");
		sb.append(monitor.getHits());
		sb.append(" TotalTime:");
		sb.append(monitor.getTotal()/1000.0);
		sb.append(" AvgTime:");
		sb.append(monitor.getAvg()/1000.0);
		
		System.out.println(sb.toString());
	}
	
	public static void dumpStatistics() {
		
		Map map = MonitorFactory.getMap();
		for(Object key : map.keySet()) {
			Monitor monitor = (Monitor) map.get(key);
			log(monitor);
		}
	}
}
