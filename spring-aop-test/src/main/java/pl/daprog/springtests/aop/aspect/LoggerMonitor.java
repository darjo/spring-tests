package pl.daprog.springtests.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect()
@Component
public class LoggerMonitor {

	@Pointcut("execution(* pl.daprog.springtests.aop.service.*.*(..))")
	private void allServiceMethods() {}

	/*
	 * Method is invoked @Before any service method
	 */
	@Before("allServiceMethods()")
	public void logEnterMethod(JoinPoint joinPoint) throws Throwable {

		String params = StringUtils.arrayToCommaDelimitedString(joinPoint.getArgs());
		
		System.out.println(joinPoint.getSignature().toShortString().replace("..", params)+" enter");
	}

	/*
	 * Method is invoked @After any service method
	 */
	@After("allServiceMethods()")
	public void logExitMethod(JoinPoint joinPoint) throws Throwable {

		System.out.println(joinPoint.getSignature().toShortString()+" exit");
	}

}
