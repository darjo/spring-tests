package pl.daprog.springtests.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.daprog.springtests.datajpa.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>, CustomerRepositoryCustom {

	/* repository generates query based on method name */
    List<Customer> findByLastName(String lastName);
    
    /* use specified query */
    @Query("select c from Customer c where lower(c.firstName) = ?")
	List<Customer> findByFirstName(String firstName);
}
