package pl.daprog.springtests.datajpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.daprog.springtests.datajpa.model.Customer;
import pl.daprog.springtests.datajpa.model.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

	List<Document> findByCustomer(Customer customer);
}
