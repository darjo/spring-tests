package pl.daprog.springtests.datajpa;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.daprog.springtests.datajpa.model.Customer;
import pl.daprog.springtests.datajpa.model.Document;
import pl.daprog.springtests.datajpa.repository.CustomerRepository;
import pl.daprog.springtests.datajpa.repository.DocumentRepository;

@Configuration
public class Application {
    
    public static void main(String[] args) {

    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        CustomerRepository customerRepository = context.getBean(CustomerRepository.class);
        DocumentRepository documentRepository = context.getBean(DocumentRepository.class);
        
        // save a couple of customers
        if(customerRepository.count()==0L) {
	        customerRepository.save(new Customer("Jack", "Bauer"));
	        customerRepository.save(new Customer("Chloe", "O'Brian"));
	        customerRepository.save(new Customer("Kim", "Bauer"));
	        customerRepository.save(new Customer("David", "Palmer"));
	        customerRepository.save(new Customer("Michelle", "Dessler"));
        }
        
        if(documentRepository.count()==0L) {
	        Customer x = customerRepository.findOne(1L);
	        documentRepository.save(new Document("1/1/2014", x));
	        documentRepository.save(new Document("2/1/2014", x));
	        Customer x2 = customerRepository.findOne(2L);
	        documentRepository.save(new Document("1/1/2014", x));
	        documentRepository.save(new Document("3/1/2014", x2));
        }
        
        // fetch all customers
        Iterable<Customer> customers = customerRepository.findAll();
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : customers) {
            System.out.println(customer);
        }
        System.out.println();

        // fetch an individual customer by ID
        Customer customer = customerRepository.findOne(1L);
        System.out.println("Customer found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(customer);
        System.out.println();

        // fetch customers by last name
        List<Customer> bauers = customerRepository.findByLastName("Bauer");
        System.out.println("Customer found with findByLastName('Bauer'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : bauers) {
            System.out.println(bauer);
        }

        // fetch customers by first name, custom query
        List<Customer> firsts = customerRepository.findByFirstName("jack");
        System.out.println("Customer found with findByFirstName('Jack'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : firsts) {
            System.out.println(bauer);
        }

        // fetch customers by first name, custom query
        List<Customer> cus = customerRepository.useCustomMethod("J");
        System.out.println("Customer found with findByCustomMethod('J'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : cus) {
            System.out.println(bauer);
        }
        
        // fetch customers by first name, dsl query
        List<Customer> dsl = customerRepository.findUsingDslQuery("C");
        System.out.println("Customer found with findUsingDslQuery('C'):");
        System.out.println("--------------------------------------------");
        for (Customer bauer : dsl) {
            System.out.println(bauer);
        }


        // fetch documents by customer
        List<Document> docs = documentRepository.findByCustomer(customerRepository.findOne(1L));
        System.out.println("Documents found with findByCustomer():");
        System.out.println("--------------------------------------------");
        for (Document doc : docs) {
            System.out.println(doc);
        }

        context.close();
    }

}
