package pl.daprog.springtests.datajpa.repository;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaMetamodelEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;

import com.mysema.query.jpa.impl.JPAQuery;

import pl.daprog.springtests.datajpa.model.Customer;
import pl.daprog.springtests.datajpa.model.QCustomer;

/**
 * Repository custom methods implementation, interface should be named *RepositoryCustom,
 * implementation - *RespositoryImpl
 * 
 * @author Darek
 *
 */
public class CustomerRepositoryImpl implements CustomerRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;
    
    public CustomerRepositoryImpl() {

    }

    /* custom method */ 
    @SuppressWarnings("unchecked")
	@Override
    public List<Customer> useCustomMethod(String param) {

    	Query query = entityManager.createQuery("SELECT c FROM Customer c WHERE c.firstName LIKE :param");
    	query.setParameter("param", param+"%");
    	return query.getResultList();
    }
    
    /* using of JPA queries */ 
    @Override
    public List<Customer> findUsingDslQuery(String param) {
    	
    	QCustomer customer = QCustomer.customer;
    	
    	JPAQuery query = new JPAQuery(entityManager);
    	return query.from(customer).where(customer.firstName.startsWith(param)).list(customer);
    }


}
