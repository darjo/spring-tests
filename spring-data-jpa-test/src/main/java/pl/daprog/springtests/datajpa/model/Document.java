package pl.daprog.springtests.datajpa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Document {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String documentNumber;
    
    @PrimaryKeyJoinColumn
    @ManyToOne
    private Customer customer;


    protected Document() {}

    public Document(String documentNumber, Customer customer) {
        this.documentNumber = documentNumber;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return String.format(
                "Document[id=%d, documentNumber='%s', customer='%s']",
                id, documentNumber, customer);
    }

}