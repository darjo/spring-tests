package pl.daprog.springtests.datajpa.repository;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import pl.daprog.springtests.datajpa.model.Customer;

/**
 * Add custom methods to CustomerRepository
 * 
 * @author Darek
 *
 */
public interface CustomerRepositoryCustom {

    List<Customer> useCustomMethod(String param);
  
    List<Customer> findUsingDslQuery(String param);
    
}
