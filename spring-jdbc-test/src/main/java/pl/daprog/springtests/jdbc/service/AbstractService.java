package pl.daprog.springtests.jdbc.service;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import pl.daprog.springtests.jdbc.model.Good;

public abstract class AbstractService<T>  {
	
	protected NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public abstract void update(T object);
	
	public abstract Integer insert(T object);
	
	public abstract T getById(Integer id);
	
	public abstract List<T> getAll();

}
