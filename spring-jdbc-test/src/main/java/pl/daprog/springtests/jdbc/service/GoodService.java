package pl.daprog.springtests.jdbc.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import pl.daprog.springtests.jdbc.model.Good;

@Service
public class GoodService extends AbstractService<Good> {
	
	private RowMapper<Good> getMapper() {
		return new RowMapper<Good>() {
			public Good mapRow(ResultSet rs, int rowNum) throws SQLException {
				Good good = new Good();
				good.idKartoteka = rs.getInt("id_kartoteka");
				good.index = rs.getString("indeks");
				good.longName = rs.getString("nazwadl");
				good.shortName = rs.getString("nazwaskr");
				return good;
			}
		};
	}
	
	public Good getByIndex(String index) {

		String sql = "select id_kartoteka, indeks, nazwadl, nazwaskr from KARTOTEKA"
				+ " where indeks = :indeks";

		SqlParameterSource namedParameters = new MapSqlParameterSource("indeks", index);
		return (Good) jdbcTemplate.queryForObject(sql, namedParameters, getMapper());
	}

	@Override
	public Good getById(Integer id) {

		String sql = "select id_kartoteka, indeks, nazwadl, nazwaskr from KARTOTEKA"
				+ " where id_kartoteka = :idKartoteka";

		SqlParameterSource namedParameters = new MapSqlParameterSource("idKartoteka", id);
		return (Good) jdbcTemplate.queryForObject(sql, namedParameters, getMapper());
	}
	
	@Override
	public List<Good> getAll() {

		String sql = "select id_kartoteka, indeks, nazwadl, nazwaskr from KARTOTEKA";
		return jdbcTemplate.query(sql, getMapper());
	}
	
	@Override
	public Integer insert(Good object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Good object) {
		
		String sql = "update kartoteka set indeks = :indeks, nazwadl = :nazwadl, nazwaskr = :nazwaskr where id_kartoteka = :idKartoteka";
		SqlParameterSource namedParameters = new MapSqlParameterSource("idKartoteka", object.idKartoteka).addValue("indeks", object.index).addValue("nazwaskr", object.shortName).addValue("nazwadl", object.longName);
		jdbcTemplate.update(sql, namedParameters);

	}



}
