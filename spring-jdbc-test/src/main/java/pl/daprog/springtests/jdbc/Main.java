package pl.daprog.springtests.jdbc;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import pl.daprog.springtests.jdbc.model.Good;
import pl.daprog.springtests.jdbc.service.GoodService;

@Configuration
public class Main {

	@Autowired
	public GoodService goodService;

	static Logger logger = Logger.getLogger(Main.class);

	private void debugBeans() {

		logger.debug("Beans list");
		for (String beanName : ApplicationContextProvider
				.getApplicationContext().getBeanDefinitionNames()) {
			logger.debug("- " + beanName);
		}
		logger.debug("----");

	}

	public void run() {

		debugBeans();

		Good x = goodService.getByIndex("014004");
		x.longName = x.shortName + " " + new Date().toString();
		goodService.update(x);
		
		Good x1 = goodService.getByIndex("014004");
		System.out.println("Kartoteka "+x1.longName);
		
		List<Good> goods = goodService.getAll();
		for(Good g : goods) {
			System.out.println("Kartoteka "+g.longName);
		}

	}

	public static void main(String[] args) {
		// inicjalizacja spring
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"Beans.xml");

		// inicjalizacja log4j
		Properties props = new Properties();
		try {
			props.load(Main.class.getResourceAsStream("/log4j.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PropertyConfigurator.configure(props);

		Main m = context.getBean(Main.class);
		m.run();

	}

}
