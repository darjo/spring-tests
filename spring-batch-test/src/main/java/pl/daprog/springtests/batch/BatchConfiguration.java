package pl.daprog.springtests.batch;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;

import pl.daprog.springtests.batch.model.Report;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration extends DefaultBatchConfigurer {

	/* ItemReader - reads from csv files */
	
    @Bean
    public ItemReader<Report> reader() {
        FlatFileItemReader<Report> reader = new FlatFileItemReader<Report>();
        reader.setResource(new ClassPathResource("cvs/report.csv"));
        reader.setLinesToSkip(1);
        reader.setLineMapper(new DefaultLineMapper<Report>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "date", "impressions", "clicks", "earning" });
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Report>() {{
                setTargetType(Report.class);
            }});
        }});
        return reader;
    }

    @Bean
    public ItemProcessor<Report, Report> processor() {
        return new ReportItemProcessor();
    }

	/* ItemWriter - inserts records to database (other than used by batch process) */

    @Bean
    public ItemWriter<Report> writer(DataSource dataSourceForWriter) {
    	
        JdbcBatchItemWriter<Report> writer = new JdbcBatchItemWriter<Report>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Report>());
        writer.setSql("INSERT INTO xxx_batch (xdate, impressions, clicks, earning) VALUES (:date, :impressions, :clicks, :earning)");
        writer.setDataSource(dataSourceForWriter);
        return writer;
    }

    /* Job configuration */
    
    @Bean
    public Job importUserJob(JobBuilderFactory jobs, Step s1) {
        return jobs.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(s1)
                .end()
                .build();
    }

    @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory, ItemReader<Report> reader,
            ItemWriter<Report> writer, ItemProcessor<Report, Report> processor) {
        return stepBuilderFactory.get("step1")
                .<Report, Report> chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean
    public JobLauncher jobLaucher(JobRepository jobRepository) {
    	SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
    	jobLauncher.setJobRepository(jobRepository);
    	return jobLauncher;
    }

    /* database for writer */

    @Bean
    public DataSource dataSourceForWriter() {
    	
    	org.firebirdsql.pool.FBWrappingDataSource pool = 
    			 new org.firebirdsql.pool.FBWrappingDataSource();
    			 pool.setMaxPoolSize(5);
    			 pool.setMinPoolSize(2);
    			 pool.setMaxStatements(10);
    			 pool.setMaxIdleTime(30 * 60 * 60);
    			 pool.setDatabase("localhost/3050:F:/Dane/Firebird/CENTROF.GDB");
    			 pool.setUserName("SYSDBA");
    			 pool.setPassword("masterkey");
    			 
    	return pool;

    }

    @Bean
    public JdbcTemplate jdbcTemplateForWriter(DataSource dataSourceForWriter) {
        return new JdbcTemplate(dataSourceForWriter);
    }

    
    /* Batch internal database datasource */
    
    @Bean
    public EmbeddedDatabase dataSource() throws Exception {
    	return new EmbeddedDatabaseBuilder()
	        .setType(EmbeddedDatabaseType.HSQL)
	        .addScript("classpath:org/springframework/batch/core/schema-drop-hsqldb.sql")
	        .addScript("classpath:org/springframework/batch/core/schema-hsqldb.sql")
	        .build();
    }
    
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    /* Custom DefaultBatchConfigurer implementation - needed for using more than one datasource */
    
    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        if(dataSource != null) {
            super.setDataSource(dataSource);
        }
    }
    
    @Override
    protected JobRepository createJobRepository() throws Exception {
        MapJobRepositoryFactoryBean factory = 
            new MapJobRepositoryFactoryBean();
        factory.afterPropertiesSet();
        return  (JobRepository) factory.getObject();
    }
}
