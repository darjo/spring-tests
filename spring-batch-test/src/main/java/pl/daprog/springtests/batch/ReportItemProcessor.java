package pl.daprog.springtests.batch;

import org.springframework.batch.item.ItemProcessor;

import pl.daprog.springtests.batch.model.Report;

public class ReportItemProcessor  implements ItemProcessor<Report, Report> {

    @Override
    public Report process(final Report report) throws Exception {

        final Report transformedReport = new Report();
        transformedReport.setClicks(report.getClicks()+"!");
        transformedReport.setDate(report.getDate());
        transformedReport.setEarning(report.getEarning());
        transformedReport.setImpressions(report.getImpressions());

        return transformedReport;
    }

}