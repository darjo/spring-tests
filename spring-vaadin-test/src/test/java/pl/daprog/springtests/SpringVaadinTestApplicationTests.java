package pl.daprog.springtests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.daprog.springtests.vaadin.SpringVaadinTestApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringVaadinTestApplication.class)
@WebAppConfiguration
public class SpringVaadinTestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
