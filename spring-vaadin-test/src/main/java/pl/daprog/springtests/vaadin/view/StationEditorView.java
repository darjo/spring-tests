package pl.daprog.springtests.vaadin.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import pl.daprog.springtests.vaadin.entity.Station;
import pl.daprog.springtests.vaadin.repository.StationRepository;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@UIScope
@SpringView(name = StationEditorView.VIEW_NAME)
public class StationEditorView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "station";

	@Autowired
	private StationRepository repository;

	/**
	 * The currently edited customer
	 */
	private Station station;

	private String backUrl;

	/* Fields to edit properties in Customer entity */
	TextField code;
	TextField name;

	/* Action buttons */
	Button save = new Button("Save", FontAwesome.SAVE);
	Button cancel = new Button("Cancel");
	CssLayout actions = new CssLayout(save, cancel);

	public StationEditorView() {

		code = new TextField("Code");
		code.setNullRepresentation("");
		code.setNullSettingAllowed(true);

		name = new TextField("Name");
		name.setNullRepresentation("");
		name.setNullSettingAllowed(true);

		addComponents(code, name, actions);

		// Configure and style components
		setSpacing(true);
		setMargin(true);

		actions.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

		// wire action buttons to save, delete and reset
		save.addClickListener(e -> { repository.save(station); getUI().getNavigator().navigateTo(backUrl); } );
		cancel.addClickListener(e -> getUI().getNavigator().navigateTo(backUrl));
	}

	@Override
	public void enter(ViewChangeEvent event) {

		// pobranie id z parametru
		Long idStation = null;

		if(!StringUtils.isEmpty(event.getParameters())) {
			idStation = Long.parseLong(event.getParameters());
		}

		if (idStation!=null) {
			// Find fresh entity for editing
			station = repository.findOne(idStation);
		}
		else {
			station = new Station();
		}

		// Bind customer properties to similarly named fields
		// Could also use annotation or "manual binding" or programmatically
		// moving values from fields to entities before saving
		BeanFieldGroup.bindFieldsUnbuffered(station, this);

		// A hack to ensure the whole form is visible
		save.focus();
		// Select all text in firstName field automatically
		code.selectAll();
	}

	public void setBackUrl(String backUrl) {

		// Mozna bezpiecznie bo widok ma zasieg UI
		this.backUrl = backUrl;
	}

}
