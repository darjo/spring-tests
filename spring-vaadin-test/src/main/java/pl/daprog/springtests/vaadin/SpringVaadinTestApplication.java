package pl.daprog.springtests.vaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringVaadinTestApplication {
	
	

	public static void main(String[] args) {
		SpringApplication.run(SpringVaadinTestApplication.class, args);
	}
}
