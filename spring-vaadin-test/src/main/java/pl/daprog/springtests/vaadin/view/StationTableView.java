package pl.daprog.springtests.vaadin.view;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.vaadin.dialogs.ConfirmDialog;

import pl.daprog.springtests.vaadin.Greeter;
import pl.daprog.springtests.vaadin.entity.Station;
import pl.daprog.springtests.vaadin.repository.StationRepository;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * UIScope - one instance of view per session, without annotation - new instance
 * when view is opened
 *
 * @author dariu_000
 *
 */
@UIScope
@SpringView(name = StationTableView.VIEW_NAME)
public class StationTableView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "stations";

	@Autowired
	private StationRepository stationRepository;

	@Autowired
	private StationEditorView stationEditorView;

	private final Grid grid;

	private final TextField filter;

	private final Button btnNew;
	private final Button btnEdit;
	private final Button btnDelete;

	public StationTableView() {
		this.grid = new Grid();
		this.filter = new TextField();
		this.btnNew = new Button("New", FontAwesome.PLUS);
		this.btnEdit = new Button("Edit", FontAwesome.EDIT);
		this.btnDelete = new Button("Delete", FontAwesome.TRASH_O);
	}

	@PostConstruct
	void init() {
		// build layout
		HorizontalLayout actions = new HorizontalLayout(filter, btnNew, btnEdit, btnDelete);

		addComponent(actions);
		addComponent(grid);

		// Configure layouts and components
		actions.setSpacing(true);
		setMargin(true);
		setSpacing(true);

		grid.setWidth(100, Unit.PERCENTAGE);
		grid.setHeight(300, Unit.PIXELS);
		grid.setColumns("code", "name");

		filter.setInputPrompt("Filter");

		// Hook logic to components

		filter.addTextChangeListener(e -> refreshGrid(e.getText()));

		btnNew.addClickListener(e -> newAction());
		btnEdit.addClickListener(e -> editAction());
		btnDelete.addClickListener(e -> deleteAction());
	}

	private Long getSelectedId() {
		if (grid.getSelectedRow() != null) {
			return ((Station) grid.getSelectedRow()).getIdStation();
		} else {
			return null;
		}
	}

	private void editAction() {

		Long idStation = getSelectedId();
		if (idStation == null) {
			return;
		}
		stationEditorView.setBackUrl(VIEW_NAME);
		this.getUI().getNavigator().navigateTo(StationEditorView.VIEW_NAME + "/" + idStation);
	}

	private void newAction() {

		stationEditorView.setBackUrl(VIEW_NAME);
		this.getUI().getNavigator().navigateTo(StationEditorView.VIEW_NAME);
	}

	private void deleteAction() {

		Long idStation = getSelectedId();
		if (idStation == null) {
			return;
		}

		ConfirmDialog.show(getUI(), "Potwierdz", "Usunąc?", "Tak", "Nie", new ConfirmDialog.Listener() {

			@Override
			public void onClose(ConfirmDialog dialog) {

				if (dialog.isConfirmed()) {

					stationRepository.delete(idStation);
					refreshGrid(null);
				}
			}
		});
	}

	private void refreshGrid(String text) {

		if (StringUtils.isEmpty(text)) {
			grid.setContainerDataSource(new BeanItemContainer<Station>(Station.class, stationRepository.findAll()));
		} else {
			grid.setContainerDataSource(new BeanItemContainer<Station>(Station.class, stationRepository
					.findByNameContaining(text)));
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {

		// Initialize listing
		refreshGrid(null);
	}
}
