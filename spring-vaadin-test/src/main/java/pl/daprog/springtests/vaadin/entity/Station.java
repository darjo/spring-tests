package pl.daprog.springtests.vaadin.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
public class Station {

	@Id
	@GeneratedValue
	private Long idStation;

	@NotNull
	private String code;

	@NotNull
	private String name;

	public Long getIdStation() {
		return idStation;
	}

	public void setIdStation(long idStation) {
		this.idStation = idStation;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Station[id=%d, code='%s', name='%s']", idStation, code, name);
	}

	@Override
	public int hashCode() {

		// bez id!
		// see
		// https://docs.jboss.org/hibernate/stable/core.old/reference/en/html/persistent-classes-equalshashcode.html

		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Station)) {
			return false;
		}
		Station other = (Station) obj;
		return new EqualsBuilder().appendSuper(super.equals(obj)).append(code, other.code).append(name, other.name)
				.isEquals();
	}
}
