package pl.daprog.springtests.vaadin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.daprog.springtests.vaadin.entity.Station;

public interface StationRepository extends JpaRepository<Station, Long> {

	List<Station> findByNameContaining(final String name);
}
