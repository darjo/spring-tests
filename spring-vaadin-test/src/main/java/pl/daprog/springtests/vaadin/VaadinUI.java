package pl.daprog.springtests.vaadin;

import org.springframework.beans.factory.annotation.Autowired;

import pl.daprog.springtests.vaadin.view.StationTableView;
import pl.daprog.springtests.vaadin.view.ViewScopedView;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@Theme("valo")
@SpringUI
public class VaadinUI extends UI {

	@Autowired
	private SpringViewProvider viewProvider;

	@Override
	protected void init(VaadinRequest request) {

		final VerticalLayout root = new VerticalLayout();
		root.setSizeFull();
		root.setMargin(true);
		root.setSpacing(true);
		setContent(root);

		final MenuBar navigationBar = new MenuBar();
		MenuItem dictMenu = navigationBar.addItem("Slowniki", null);
		createNavigationMenuItem(dictMenu, "Stacje", StationTableView.VIEW_NAME);
		createNavigationMenuItem(dictMenu, "View Scoped View", ViewScopedView.VIEW_NAME);
		root.addComponent(navigationBar);

		final Panel viewContainer = new Panel();
		viewContainer.setSizeFull();
		root.addComponent(viewContainer);
		root.setExpandRatio(viewContainer, 1.0f);

		Navigator navigator = new Navigator(this, viewContainer);
		navigator.addProvider(viewProvider);
	}

	private void createNavigationMenuItem(MenuItem parent, String caption, final String viewName) {

		MenuItem item = parent.addItem(caption, clicked -> getUI().getNavigator().navigateTo(viewName));
	}
}
